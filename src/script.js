

function throttle(func, delay) {
    let lastCalledTime = 0;
    return function (...args) {
        const currentTime = Date.now();
        if (currentTime - lastCalledTime >= delay) {
            func.apply(this, args);
            lastCalledTime = currentTime;
        }
    };
}

function myEventHandler() {
    // console.log('Event handler invoked');
}

const throttledEventHandler = throttle(myEventHandler, 1000); // Throttle to 1 second

// Attach the throttled event handler to an event listener
window.addEventListener('scroll', throttledEventHandler);


function closeOffcanvas() {
    var navbarCollapse = document.getElementById('navbarCollapse');
    navbarCollapse.classList.add('hidden');
    var navbarTogglerButton = document.getElementById('navbarToggler');
    navbarTogglerButton.classList.remove('navbarTogglerActive')
}

function toggleOffCanvas() {

    //check if offcanvas is active
    var navbarCollapse = document.getElementById('navbarCollapse');
    var navbarTogglerButton = document.getElementById('navbarToggler');


    if (navbarTogglerButton.classList.contains('navbarTogglerActive')) {
        navbarTogglerButton.classList.remove('navbarTogglerActive')
        navbarCollapse.classList.add('hidden');
    }
    else {
        navbarTogglerButton.classList.add('navbarTogglerActive')
        navbarCollapse.classList.remove('hidden');
    }

}

function openOffcanvas() {
    var navbarTogglerButton = document.getElementById('navbarToggler');
    navbarTogglerButton.classList.add('navbarTogglerActive')
    var navbarCollapse = document.getElementById('navbarCollapse');
    navbarCollapse.classList.remove('hidden');
}

function handleGetInTouchBtnClick() {
    var getInTouchModal = document.getElementById('getInTouchModal');
    getInTouchModal.classList.remove('hidden')
    var getInTouchForm = document.getElementById('getInTouchForm')
    getInTouchForm.classList.remove('hidden');
    var getInTouchResponseSuccess = document.getElementById('getInTouchResponseSuccess');
    getInTouchResponseSuccess.classList.add('hidden');
    var getInTouchResponseFailure = document.getElementById('getInTouchResponseFailure');
    getInTouchResponseFailure.classList.add('hidden');
    var incompleteFormImage = document.getElementById('form-incomplete-image')
    incompleteFormImage.classList.add('hidden');
    incompleteFormImage.classList.add('lg:visible');
    var modalContent = document.getElementById("modal-content")
    modalContent.classList.remove("max-height-[468px]")


    //hide validation messages
    const errorDivs = document.querySelectorAll('p[id$="-validation-message"]');
    const inputs = document.querySelectorAll('input[type=text], input[type=email]');
    inputs.forEach(inputElement => {
        inputElement.value = '';
        inputElement.classList.remove('border-red-500')
    })

    const radioInputs = document.querySelectorAll('input[type=radio]');
    radioInputs.forEach(radioEle => {
        radioEle.checked = false;
        radioEle.classList.remove('invalid-radio')
    })
    errorDivs.forEach(errorDiv => {
        errorDiv.classList.add('hidden')
    })

}

function handleGetInTouchModalCancel() {
    var getInTouchModal = document.getElementById('getInTouchModal');
    getInTouchModal.classList.add('hidden');
    const loader = document.getElementById("getInTouchLoader");
    loader.classList.add("hidden");

    //clear validation object values and validation
    if (validation) {
        for (const property in validation) {
            validation[property].value = '';
            validation[property].isValid = false;
            if (property === 'preferredMethod') validation[property].value = false;
            // console.log(`${property}: ${validation[property]}`);
        }
    }
    var saveButtonElement = document.getElementById('save-button');
    saveButtonElement.disabled = true;
    // var getInTouchResponseSuccess = document.getElementById('getInTouchResponseSuccess');
    // incompleteFormImage.classList.add('lg:visible');

}

var validation = {
    'name': { 'required': true, value: '', isValid: false, regex: /^[a-zA-Z\s]*$/ },
    'countryCode': { 'required': true, value: '', isValid: false, regex: /^\+\d{1,3}$/ },
    'phone': { 'required': true, value: '', isValid: false, regex: /^[0-9]{10}$/, relatedField: 'countryCode' },
    'email': { 'required': true, value: '', isValid: false, regex: /\S+@\S+\.\S+/ },
    'preferredMethod': { 'required': true, value: false, isValid: false },
}

function handleBlur(e) {
    console.log("####", e.target.name, e.target.value);
    // validation[e.target.name] = e.target.value;
    //validate field
    validation[e.target.name].value = e.target.value;
    if (e.target.type !== 'radio') {
        validateFieldUpdate(e.target.name, e.target.value);
    }
    else if (e.target.type === 'radio') {
        validateRadioButton(e.target.name, e.target.checked)
    }

    //get 
    validateForm();

}
function validateForm() {
    var inValidArray = [];
    Object.keys(validation).forEach(key => {
        // console.log(key + ': ' + validation[key]);
        if (!validation[key].isValid)
            inValidArray.push(key)

    });
    // console.log("@@@", validation)
    var saveButtonElement = document.getElementById('save-button');
    if (inValidArray.length) {
        //enable submit button
        saveButtonElement.disabled = true;
    }
    else {
        saveButtonElement.disabled = false;
    }
}

function validateRadioButton(fieldName, value) {
    const radioButtons = document.getElementsByName('preferredMethod');
    const errorDiv = document.getElementById(fieldName + '-validation-message');
    if (validation[fieldName].required && !value) {
        validation[fieldName].isValid = false;
        radioButtons.forEach(radioButton => {
            radioButton.classList.add('invalid-radio');
        });
        errorDiv?.classList?.remove('hidden')
    }
    else {
        validation[fieldName].isValid = true;
        radioButtons.forEach(radioButton => {
            radioButton.classList.remove('invalid-radio');
        });
        errorDiv?.classList?.add('hidden')
    }
}

function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

function validateName(name) {
    const regex = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
    return regex.test(name)
}

function validatePhoneNumber(phoneNumber) {
    return /^\d{10,}$/.test(phoneNumber);
}

function valiateRegExp(exp, value) {
    return exp.test(value);
}

function setError(fieldName) {
    var ele = document.getElementById(fieldName + '-validation-message');
    var input = document.getElementById(fieldName);
    validation[fieldName].isValid = false;
    ele.classList.remove('hidden');
    input?.classList?.add('border-red-500')
}

function unsetError(fieldName) {
    var ele = document.getElementById(fieldName + '-validation-message');
    var input = document.getElementById(fieldName);
    validation[fieldName].isValid = true;
    ele.classList.add('hidden');
    input?.classList?.remove('border-red-500')
}

function validateFieldUpdate(fieldName, value) {
    var ele = document.getElementById(fieldName + '-validation-message');
    var input = document.getElementById(fieldName);

    //validate empty value
    if (validation[fieldName].required && !value) {
        setError(fieldName)
    }
    else {
        var isValid = validation[fieldName].regex ? valiateRegExp(validation[fieldName].regex, value) : true;
        validation[fieldName].isValid = isValid;
        if (isValid) {
            unsetError(fieldName);
        }
        else {
            setError(fieldName)
        }

    }

    if (validation[fieldName].relatedField) {
        validateFieldUpdate(validation[fieldName].relatedField, validation[validation[fieldName].relatedField].value)
    }
}

function validateField(fieldName, value) {
    var ele = document.getElementById(fieldName + '-validation-message');
    var input = document.getElementById(fieldName);


    //validate required
    if (validation[fieldName].required && !value) {
        validation[fieldName].isValid = false;
        ele.classList.remove('hidden');
        input?.classList?.add('border-red-500')
    }
    else if (validation[fieldName].required && value) {
        //check validation
        validation[fieldName].isValid = true;
        ele.classList.add('hidden');
        input?.classList?.remove('border-red-500')
    }

    //validate email
    if (validation[fieldName].required && value && fieldName === 'email' && validateEmail(value)) {
        validation[fieldName].isValid = true;
        ele.classList.add('hidden');
        input.classList.remove('border-red-500')
    }
    else if (validation[fieldName].required && value && fieldName === 'email' && !validateEmail(value)) {
        validation[fieldName].isValid = false;
        ele.classList.remove('hidden');
        input.classList.add('border-red-500')
    }

    //validate phone number

    if (validation[fieldName].required && value && fieldName === 'phone' && validatePhoneNumber(value)) {
        validation[fieldName].isValid = true;
        ele.classList.add('hidden');
        input.classList.remove('border-red-500')
    }
    else if (validation[fieldName].required && value && fieldName === 'phone' && !validatePhoneNumber(value)) {
        validation[fieldName].isValid = false;
        ele.classList.remove('hidden');
        input.classList.add('border-red-500')
    }
}

function handleGetInTouchSubmit(e) {
    e.preventDefault();
    e.stopPropagation();

    var inValidArray = [];
    Object.keys(validation).forEach(key => {
        // console.log(key + ': ' + validation[key]);
        if (!validation[key].isValid)
            inValidArray.push(key)

    });

    if (!inValidArray.length) {
        //send email with data

        //get data from validation
        var data = {};

        for (var prop in validation) {
            if (prop === 'phone') {
                data[prop] = parseInt(validation[prop].value);
            }
            else {
                data[prop] = validation[prop].value;
            }
        }

        //timeline object 
        const tl = gsap.timeline({ defaults: { ease: 'power4.out ', duration: 2, repeat: -1 } });
        //send email
        handleRequest(data, tl);

        //show loader
        var getInTouchFormElement = document.getElementById('getInTouchForm');
        var incompleteFormImage = document.getElementById('form-incomplete-image')
        getInTouchFormElement.classList.add('hidden');
        incompleteFormImage.classList.remove('lg:visible')
        incompleteFormImage.classList.add("hidden");
        //TODO - enable loader when mobile detection is in place
        // var getInTouchLoader = document.getElementById('getInTouchLoader');
        // getInTouchLoader.classList.remove('hidden');
        // playLoaderAnimation();
        //TODO - end
        // var getInTouchResponseSuccess = document.getElementById('getInTouchResponseSuccess');

        // getInTouchResponseSuccess.classList.remove('hidden')
        //TODO
        // var modalContent = document.getElementById("modal-content")
        // modalContent.classList.add("max-height-[468px]")
        // tl.set("#hand-down", { opacity: 1 })
        // tl.set("#getInTouchResponseSuccess", { width: 0 });
        //TODO
        tl.set(" #thank-you-heading ", { opacity: 0, y: 150 });
        tl.set(" #error-heading ", { opacity: 0, y: 150 });

        tl.set(" #thank-you-text ", { opacity: 0, y: -150 })
        tl.set(" #error-text", { opacity: 0, y: -150 })
        //TODO
        // tl.set('#tick,#bubble,#thumbs-up', { opacity: 0 })

        // tl.from("#progress-bar-1, #progress-bar-2", { scaleX: 0 })
        //     .from(".gray-button", { scaleX: 0 }, "-=.5");
        //TODO

        //temporary loader
        var tempLoader = document.getElementById('css-loader');
        tempLoader.classList.remove('hidden')
    }
}



function jsonToPostParameters(json) {
    return Object.keys(json)
        .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(json[key]))
        .join('&');
}

function handleRequestSuccess(tl) {
    tl.kill();
    var tempLoader = document.getElementById('css-loader');
    tempLoader.classList.add('hidden');
    var getInTouchResponseSuccess = document.getElementById('getInTouchResponseSuccess');

    getInTouchResponseSuccess.classList.remove('hidden')
    const tl1 = gsap.timeline({ defaults: { ease: 'power4.out ', duration: .5, } });
    //TODO - enable svg animation
    // tl1.to("#hand-down", { opacity: 0 })
    //     .to("#thumbs-up", { opacity: 1 }, "-=.5")
    //     .to("#bubble", { opacity: 1 }, "-=.3")
    //     .to("#tick", { opacity: 1 }, "-=.5")
    //     .to("#getInTouchResponseSuccess", { width: "100%", duration: 1 }, "-=.6");
    //TODO -enable svg animation
    // gsap.to("#thank-you-heading", { opacity: 1, y: 0 });
    tl1.to("#thank-you-heading, #thank-you-text", { opacity: 1, y: 0, stagger: .5 }, "-=.2")


    var loaderElements = document.querySelectorAll('form-loader');
    loaderElements.forEach(loaderE => {
        loaderE.classList.remove('hidden')
    })
}

function handleRequestFailure(tl) {
    tl.kill();
    var tempLoader = document.getElementById('css-loader');
    tempLoader.classList.add('hidden');
    var getInTouchResponseFailure = document.getElementById('getInTouchResponseFailure');
    getInTouchResponseFailure.classList.remove('hidden');
    const tl1 = gsap.timeline({ defaults: { ease: 'power4.out ', duration: .5, } });
    tl1.to("#error-heading, #error-text", { opacity: 1, y: 0, stagger: .5 }, "+=.2")
}

function handleRequest(data, timeline) {
    const url = 'https://2m24nm1ub7.execute-api.us-east-2.amazonaws.com/dev/contact-us';

    const xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-Type', 'application/json');

    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                const response = xhr.responseText;
                // console.log(response); // Response from the server
                // handleRequestFailure(timeline)
                handleRequestSuccess(timeline)
            } else {

                handleRequestFailure(timeline)
            }
        }
    };


    xhr.send(JSON.stringify(data));


}
//radio change handler

function handleRadioChange(event) {
    validateFieldUpdate(event.target.name, event.target.value);
    //validate form
    validateForm();
}


//for section in-view functionality

function isInViewport(el) {
    const rect = el.getBoundingClientRect();

    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.right <= (window.innerWidth || document.documentElement.clientWidth)

    );
}


const pageSections = document.querySelectorAll('.in-view-animation ');


function onScrollAnimation() {
    var inviewSection = '';
    pageSections.forEach((pageSection) => {
        // console.log("!!!", pageSection, isInViewport(pageSection))
        if (isInViewport(pageSection)) {
            inviewSection = pageSection
        }
    })

    console.log("!!!", inviewSection, inviewSection.closest('section').id);
    var sectionId = inviewSection.closest('section').id;
    var tl = gsap.timeline({ defaults: { ease: 'power4.out ', duration: .3, } });
    tl.from("#" + sectionId + " .in-view-animation .animation-top:not(.visited)", { opacity: 0, y: 150, stagger: .5 },);
    tl.from("#" + sectionId + " .in-view-animation .animation-down:not(.visited)", { opacity: 0, y: -150, stagger: .5 }, "+=.3");

    // animation - reveal
    tl.from("#" + sectionId + " .in-view-animation .animation-reveal:not(.visited)", { opacity: 0, stagger: .5 }, "+=.3");
    document.querySelectorAll("#" + sectionId + " .in-view-animation .animation-top ").forEach(item => {
        item.classList.add('visited')
    })
    document.querySelectorAll("#" + sectionId + " .in-view-animation .animation-down").forEach(item => {
        item.classList.add('visited')
    })
    document.querySelectorAll("#" + sectionId + " .in-view-animation .animation-reveal").forEach(item => {
        item.classList.add('visited')
    });
}
onScrollAnimation()


document.addEventListener('scroll', onScrollAnimation, {
    passive: true
});


//handle services popups
function handleServicesClick(service) {
    const element = document.getElementById(service);
    const width = 960;
    console.log("!!!!", service)
    if (screen.width < width) {
        if (element.classList.contains('hidden')) {
            element.classList.remove('hidden')
        }
        else {
            element.classList.add('hidden')
        }
    }
}

function closeServicesModal(service) {
    const element = document.getElementById(service + 'Modal');
    const width = 960;
    if (screen.width < width) {
        if (element.classList.contains('hidden')) {
            element.classList.remove('hidden')
        }
        else {
            element.classList.add('hidden')
        }
    }
}